import { useMutation } from "@apollo/client";
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { Link } from "react-router-dom";
import { SIGNUP } from "../utilities/graphql";
import { useHistory } from "react-router";
import { toast } from "react-toastify";

export const SignUp = () => {
  const [fields, setFields] = useState({ name: "", email: "", password: "" });
  const [signUp] = useMutation(SIGNUP);
  const history = useHistory();
  const handleChange = (e) => {
    setFields({ ...fields, [e.target.name]: e.target.value });
  };

  const handleSubmit = () => {
    signUp({
      variables: {
        name: fields.name,
        email: fields.email,
        password: fields.password,
      },
    })
      .then((res) => {
        toast.success("SignUp Successfull");
        history.push("/");
      })
      .catch((err) => {
        toast.error(err.message);
      });
  };

  return (
    <Grid spacing={2} container justifyContent="center">
      <Card sx={{ mt: 3 }}>
        <CardContent>
          <Typography variant="h5">Sign Up</Typography>
          <Box sx={{ m: 1 }}>
            <TextField
              label="Name"
              required
              name="name"
              value={fields.name}
              onChange={handleChange}
            ></TextField>
          </Box>
          <Box sx={{ m: 1 }}>
            <TextField
              required
              label="Email"
              type="email"
              name="email"
              value={fields.email}
              onChange={handleChange}
            ></TextField>
          </Box>
          <Box sx={{ m: 1 }}>
            <TextField
              required
              label="Password"
              type="password"
              name="password"
              onChange={handleChange}
            ></TextField>
          </Box>
          <Link to="/">
            <small>Already have an account?</small>
          </Link>
          <Box sx={{ m: 1 }}>
            <Button variant="contained" onClick={handleSubmit}>
              Sign Up
            </Button>
          </Box>
        </CardContent>
      </Card>
    </Grid>
  );
};
