import { useMutation, useQuery } from "@apollo/client";
import { Button, Grid, Typography } from "@mui/material";
import Container from "@mui/material/Container";
import React, { useState } from "react";
import { toast } from "react-toastify";
import {
  ADD_USER,
  DELETE_USER,
  EDIT_USER,
  GET_USERS,
} from "../utilities/graphql";
import { UserForm } from "./UserForm";
import { UserTable } from "./UserTable";

export const UserManagement = (props) => {
  const [selectedUser, setSelectedUser] = useState(null);
  const [openPopup, setOpenPopup] = useState(false);
  const { data, error, refetch } = useQuery(GET_USERS);
  const [addUser] = useMutation(ADD_USER);
  const [editUser] = useMutation(EDIT_USER);
  const [deleteUser] = useMutation(DELETE_USER);

  const handleAdd = () => {
    setSelectedUser(null);
    setOpenPopup(true);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (selectedUser.id)
      editUser({
        variables: {
          id: selectedUser.id,
          name: selectedUser.name,
          email: selectedUser.email,
          age: Number(selectedUser.age),
        },
      })
        .then((res) => {
          toast.success("User Added Successfully!!!");
          refetch();
          setSelectedUser(null);
          setOpenPopup(false);
        })
        .catch((err) => {
          toast.error(err.message);
        });
    else
      addUser({
        variables: {
          name: selectedUser.name,
          email: selectedUser.email,
          age: Number(selectedUser.age),
        },
      })
        .then(() => {
          refetch();
          setSelectedUser(null);
          setOpenPopup(false);
        })
        .catch((err) => {
          toast.error(err.message);
        });
  };

  return (
    <Container>
      <h3>User Management</h3>
      <Grid item xs={2}>
        <Button variant="contained" onClick={handleAdd}>
          {" "}
          ADD USER +
        </Button>
      </Grid>
      {!error ? (
        <UserTable
          users={data?.getUsers}
          setSelectedUser={setSelectedUser}
          setOpenPopup={setOpenPopup}
          refetch={refetch}
          deleteUser={deleteUser}
        />
      ) : (
        <Typography variant="h5">Error Loading Users...</Typography>
      )}
      <UserForm
        openPopup={openPopup}
        closePopup={() => setOpenPopup(false)}
        selectedUser={selectedUser}
        setSelectedUser={setSelectedUser}
        handleSubmit={handleSubmit}
      />
    </Container>
  );
};
