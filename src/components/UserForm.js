import { Close } from "@mui/icons-material";
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  IconButton,
} from "@mui/material";
import React from "react";

export const UserForm = ({
  openPopup,
  closePopup,
  selectedUser,
  setSelectedUser,
  handleSubmit,
}) => {
  const handleChange = (e) => {
    setSelectedUser({ ...selectedUser, [e.target.name]: e.target.value });
  };
  return (
    <Dialog open={openPopup}>
      <DialogTitle>
        Manage User
        <IconButton
          style={{ float: "right" }}
          onClick={closePopup}
          size="small"
        >
          <Close />
        </IconButton>
      </DialogTitle>

      <DialogContent>
        <Grid container spacing={2}>
          <Grid item xs={4}>
            <TextField
              variant="outlined"
              label="name"
              required
              name="name"
              value={selectedUser?.name}
              onChange={handleChange}
            ></TextField>
          </Grid>
          <Grid item xs={4}>
            <TextField
              variant="outlined"
              label="email"
              required
              name="email"
              type="email"
              value={selectedUser?.email}
              onChange={handleChange}
            ></TextField>
          </Grid>
          <Grid item xs={4}>
            <TextField
              variant="outlined"
              label="age"
              name="age"
              type="number"
              value={selectedUser?.age}
              onChange={handleChange}
            ></TextField>
          </Grid>
        </Grid>
        <Box sx={{ textAlign: "center" }}>
          <Button
            onClick={handleSubmit}
            variant="contained"
            type="submit"
            sx={{ my: 1, width: "50%" }}
          >
            Save
          </Button>
        </Box>
      </DialogContent>
    </Dialog>
  );
};
