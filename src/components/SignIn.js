import { useMutation } from "@apollo/client";
import { Box, Button, Grid, TextField, Typography } from "@mui/material";
import { useState } from "react";
import { SIGNIN } from "../utilities/graphql";
import { Link, useHistory } from "react-router-dom";
export const SignIn = () => {
  const [fields, setFields] = useState({ email: "", password: "" });
  const [signIn] = useMutation(SIGNIN);
  const history = useHistory();
  const handleFieldChange = (e) => {
    setFields({ ...fields, [e.target.name]: e.target.value });
  };

  const handleSignIn = () => {
    console.log(fields);
    signIn({
      variables: {
        email: fields.email,
        password: fields.password,
      },
    })
      .then((res) => {
        sessionStorage.setItem("token", res.data?.signIn?.token);
        history.push("/users");
      })
      .catch((err) => {});
  };
  return (
    <Grid container alignItems="center" direction="row" justifyContent="center">
      <Grid item textAlign="center">
        <Box>
          <Typography sx={{ my: 3 }} variant="h5" component="div">
            SignIn
          </Typography>
          <Grid container justifyContent="center" spacing={2}>
            <Grid item lg={12}>
              <TextField
                required
                variant="outlined"
                type="email"
                name="email"
                label="Email"
                value={fields.email}
                onChange={handleFieldChange}
              ></TextField>
            </Grid>
            <Grid item lg={12}>
              <TextField
                required
                variant="outlined"
                type="password"
                name="password"
                label="Password"
                value={fields.password}
                onChange={handleFieldChange}
              ></TextField>
            </Grid>
          </Grid>
          <Link to="/signUp">
            <small>Don't have an account? SignUp</small>
          </Link>
          <Box component="div" sx={{ mt: 2 }}>
            <Button variant="contained" onClick={handleSignIn}>
              Sign In
            </Button>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
};
