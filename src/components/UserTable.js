import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import {
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import React from "react";
import { toast } from "react-toastify";

export const UserTable = ({
  users,
  setSelectedUser,
  setOpenPopup,
  refetch,
  deleteUser,
}) => {
  const handleEdit = (user) => {
    setSelectedUser(user);
    setOpenPopup(true);
  };

  const handleDelete = (user) => {
    const response = window.confirm(
      `Do you want to delete user "${user.name}"?`
    );
    if (response)
      deleteUser({ variables: { id: user.id } })
        .then(() => {
          refetch();
        })
        .catch((err) => {
          toast.error(err.message);
        });
  };

  return (
    <Table spacing={1}>
      <TableHead>
        <TableRow>
          <TableCell>Action</TableCell>
          <TableCell>ID</TableCell>
          <TableCell>Name</TableCell>
          <TableCell>Email</TableCell>
          <TableCell>Age</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {users?.map((user) => (
          <TableRow key={user.id}>
            <TableCell>
              <IconButton onClick={() => handleEdit(user)}>
                <EditIcon fontSize="small"></EditIcon>
              </IconButton>
              <IconButton>
                <DeleteIcon
                  fontSize="small"
                  onClick={() => handleDelete(user)}
                ></DeleteIcon>
              </IconButton>
            </TableCell>
            <TableCell>{user.id}</TableCell>
            <TableCell>{user.name}</TableCell>
            <TableCell>{user.email}</TableCell>
            <TableCell>{user.age}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};
