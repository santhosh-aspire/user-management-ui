import { gql } from "@apollo/client";

export const GET_USERS = gql`
  query {
    getUsers {
      id
      name
      email
      age
    }
  }
`;

export const ADD_USER = gql`
  mutation AddUser($name: String!, $email: String!, $age: Int) {
    addUser(name: $name, email: $email, age: $age) {
      id
    }
  }
`;

export const EDIT_USER = gql`
  mutation UpdateUser($id: Int!, $name: String, $email: String, $age: Int) {
    updateUser(user: { id: $id, name: $name, email: $email, age: $age }) {
      id
      name
    }
  }
`;

export const DELETE_USER = gql`
  mutation DeleteUser($id: Float!) {
    removeUser(id: $id)
  }
`;

export const SIGNIN = gql`
  mutation SignIn($email: String!, $password: String!) {
    signIn(email: $email, password: $password) {
      token
    }
  }
`;

export const SIGNUP = gql`
  mutation SignUp(
    $name: String!
    $email: String!
    $password: String!
    $age: Int
  ) {
    signUp(name: $name, email: $email, password: $password, age: $age) {
      id
    }
  }
`;
