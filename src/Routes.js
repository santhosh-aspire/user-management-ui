import { Route, Switch } from "react-router-dom";
import { SignIn } from "./components/SignIn";
import { SignUp } from "./components/SignUp";
import { UserManagement } from "./components/UserManagement";

export const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={SignIn} />
      <Route path="/users" exact component={UserManagement} />
      <Route path="/signUp" component={SignUp}></Route>
    </Switch>
  );
};
